package com.gxuwz.repository;

import com.gxuwz.SellApplication;
import com.gxuwz.entity.OrderDetail;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author _Artisan
 * @version V1.0
 * @date 2017/12/12 11:20
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SellApplication.class)
public class OrderDetailRepositoryTest {

    @Autowired
    private OrderDetailRepository repository;

    @Test
    public void testSave(){
        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setDetailId("123456710");
        orderDetail.setOrderId("123457");
        orderDetail.setProductIcon("http://xxx.jsp");
        orderDetail.setProductId("1111120");
        orderDetail.setProductName("皮蛋粥");
        orderDetail.setProductPrice(new BigDecimal(2.3));
        orderDetail.setProductQuantity(3);

        OrderDetail result =  repository.save(orderDetail);
        Assert.assertNotNull(result);
    }

    @Test
    public void findByOrderId() {
        List<OrderDetail> orderDetailList = repository.findByOrderId("123456");
        assertNotEquals(0,orderDetailList.size());
    }
}