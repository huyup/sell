package com.gxuwz.repository;

import com.gxuwz.entity.ProductCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductCategoryRepositoryTest {

    /**
     * 注意：SpringBoot项目的Bean装配默认规则是根据Application类所在的包位置从上往下扫描！
     * 如果Application类所在的包为：com.boot.app，则只会扫描com.boot.app包及其所有子包，
     * 如果service或dao所在包不在com.boot.app及其子包下，则不会被扫描！
     * 即, 把Application类放到dao、service所在包的上级，com.boot.Application
     */
    @Autowired
    private ProductCategoryRepository repository;

    @Test
    public void findOneTest(){
        ProductCategory productCategory = repository.findOne(1);
        System.out.println(productCategory);
    }

    @Test
    public void saveTest(){
        ProductCategory productCategory = new ProductCategory("女生最爱",6);
        ProductCategory result  = repository.save(productCategory);
        Assert.assertNotNull(result);
    }

    @Test
    public void updateTest(){

    }

    @Test
    public void findByCategoryTypeInTest(){
        List<Integer> list = Arrays.asList(2,3,4);
        List<ProductCategory> result = repository.findByCategoryTypeIn(list);
        Assert.assertNotEquals(0,result.size());
    }

}