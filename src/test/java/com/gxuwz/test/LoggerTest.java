package com.gxuwz.test;

import com.gxuwz.SellApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SellApplication.class)
@Slf4j
public class LoggerTest {

    @Test
    public void test1(){
        /** 测试使用idea 提交 */
        String name = "hyp";
        String password = "123456";

        // 需要安装 lombok 插件
        log.debug("debug...");
        log.info("info...");
        log.info("name : {}, password : {}",name,password);
        log.error("error...");

    }
}
