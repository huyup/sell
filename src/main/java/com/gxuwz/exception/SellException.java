package com.gxuwz.exception;

import com.gxuwz.enums.ResultEnum;

/**
 * @author _Artisan
 * @version V1.0
 * @date 2017/12/15 20:02
 */
public class SellException extends RuntimeException{

    private Integer code;

    public SellException(ResultEnum resultEnum){
        super(resultEnum.getMessage());
        this.code = resultEnum.getCode();
    }

    public SellException(Integer code,String message){
        super(message);
        this.code = code;
    }
}
