package com.gxuwz.util;

import java.util.Random;

/**
 * @author _Artisan
 * @version V1.0
 * @date 2017/12/15 20:15
 */
public class KeyUtil {

    /**
     * 生成唯一的主键
     * 格式：时间+随机数
     * @return
     */
    public synchronized static String genUniqueKey(){
        Random random = new Random();
        Integer number = random.nextInt(900000) + 100000;
        return System.currentTimeMillis() + String.valueOf(number);
    }
}
