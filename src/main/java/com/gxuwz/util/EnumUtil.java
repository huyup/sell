package com.gxuwz.util;

import com.gxuwz.enums.CodeEnum;

/**
 * @author _Artisan
 * @version V1.0
 * @date 2017/12/30 17:23
 */
public class EnumUtil {

    /**
     * 方便在页面显示状态的具体信息
     * @param code
     * @param enumClass
     * @param <T>
     * @return
     */
    public static <T extends CodeEnum> T getByCode(Integer code, Class<T> enumClass){
        for (T each:enumClass.getEnumConstants()) {
            if(code.equals(each.getCode())){
                return each;
            }
        }
        return null;
    }

}
