package com.gxuwz.service;

import com.gxuwz.dto.OrderDTO;

/**
 * @author _Artisan
 * @version V1.0
 * @date 2017/12/22 10:28
 */
public interface BuyerService {

    //查询一个订单
    OrderDTO findOrderOne(String openid, String orderId);

    //取消订单
    OrderDTO cancelOrder(String openid, String orderId);
}
