package com.gxuwz.repository;

import com.gxuwz.entity.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author _Artisan
 * @version V1.0
 * @date 2017/12/9 22:28
 */
public interface OrderDetailRepository extends JpaRepository<OrderDetail,String>{

    List<OrderDetail> findByOrderId(String orderId);
}
