package com.gxuwz.repository;

import com.gxuwz.entity.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author _Artisan
 * @version V1.0
 * @date 2017/12/8 12:15
 */
public interface ProductInfoRepository extends JpaRepository<ProductInfo,String> {
    List<ProductInfo> findByProductStatus(Integer productStatus);
}
