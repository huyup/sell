package com.gxuwz.enums;

/**
 * @author _Artisan
 * @version V1.0
 * @date 2017/12/30 17:22
 */
public interface CodeEnum {
    Integer getCode();
}
