package com.gxuwz.enums;

import lombok.Getter;

/**
 * 订单状态枚举
 * @author _Artisan
 * @version V1.0
 * @date 2017/12/9 22:13
 */
@Getter
public enum OrderStatusEnum implements CodeEnum{
    NEW(0,"新订单"),
    FINISHED(1,"已完结"),
    CANCEL(2,"已取消")
    ;

    private Integer code;
    private String message;

    OrderStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
