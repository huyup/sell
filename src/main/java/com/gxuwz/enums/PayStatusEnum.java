package com.gxuwz.enums;

import lombok.Getter;

/**
 * 支付状态枚举
 * @author _Artisan
 * @version V1.0
 * @date 2017/12/9 22:15
 */
@Getter
public enum PayStatusEnum implements CodeEnum{
    WAIT(0,"等待支付"),
    SUCCESS(1,"支付成功")
    ;

    private Integer code;
    private String message;

    PayStatusEnum(Integer code,String message){
        this.code = code;
        this.message = message;
    }
}
