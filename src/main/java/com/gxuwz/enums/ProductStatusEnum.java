package com.gxuwz.enums;

import lombok.Getter;

/**
 * 商品状态
 * @author _Artisan
 * @version V1.0
 * @date 2017/12/8 18:09
 */
@Getter
public enum ProductStatusEnum implements CodeEnum{
    UP(0,"在架"),
    DOWN(1,"不在架");

    private Integer code;
    private String message;

    ProductStatusEnum(Integer code,String message){
        this.code = code;
        this.message = message;
    }
}
