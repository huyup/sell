package com.gxuwz.dto;

import lombok.Data;

/**
 * 购物车
 * @author _Artisan
 * @version V1.0
 * @date 2017/12/15 20:29
 */
@Data
public class CartDTO {

    /** 商品ID. */
    private String productId;

    /** 数量. */
    private Integer productQuantity;

    public CartDTO(String productId, Integer productQuantity) {
        this.productId = productId;
        this.productQuantity = productQuantity;
    }

}
