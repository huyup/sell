package com.gxuwz.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * 订单详情
 * @author _Artisan
 * @version V1.0
 * @date 2017/12/9 22:06
 */
@Entity
@Data
public class OrderDetail {

    @Id
    private String detailId;

    /** 订单ID. */
    private String orderId;

    /** 商品ID. */
    private String productId;

    /** 商品名字. */
    private String productName;

    /** 单价. */
    private BigDecimal productPrice;

    /** 商品数量. */
    private Integer productQuantity;

    /** 商品小图. */
    private String productIcon;
}
