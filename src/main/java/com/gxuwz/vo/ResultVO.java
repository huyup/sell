package com.gxuwz.vo;

import lombok.Data;

/**
 * http请求返回的最外层对象
 * @author _Artisan
 * @version V1.0
 * @date 2017/12/8 22:12
 */
@Data
public class ResultVO<T> {

    /** 错误码. */
    private Integer code;

    /** 提示信息. */
    private String msg;

    /** 具体内容. */
    private T data;
}
