package com.gxuwz.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * 商品（包含类目）
 * @author _Artisan
 * @version V1.0
 * @date 2017/12/8 22:17
 */
@Data
public class ProductVO {

    @JsonProperty("name") // 返回前端的数据格式
    private String categoryName;

    @JsonProperty("type")
    private Integer categoryType;

    // 为了安全性和隐私性的考虑，前台需要什么数据就返回什么数据
    @JsonProperty("foods")
    private List<ProductInfoVO> productInfoVOList;

}
