package com.gxuwz.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author _Artisan
 * @version V1.0
 * @date 2017/12/27 10:15
 */
@RestController
@RequestMapping("/weixin")
@Slf4j
public class WeixinController {

    /********************************************/
    /*****没有微信服务号,暂停此部分的学习*******/
    /******************************************/

    @GetMapping("/auth")
    public void auth(@RequestParam("code")String code){
        log.info("进入auth的方法...");
        log.info("code={}",code);
    }

}
